# Test

## Utiliser l'app
Exécutez <br />
``
docker-compose build
``
<br />
``
docker-compose up -d
``

App : http://localhost:8080/
<br />
Maihog : http://localhost:8025/

Commande pour envoyer l'historique par mail : <br/>
``bin/console bin/console send-history`` <br/>
Sur un serveur nous devrons mettre un cron en place pour l'envoie


## Démarche avec plus de temps
- Connexion avec une API externe pour avoir les taux de change en direct
- Back avec une route API et un front (vuejs par exemple) pour avoir une app moderne et sans reload
- Ajout d'un select pour connaitre la devise de la réponse du calcul. Possibilité de faire 100USD + 50EUR et avoir le résultat en CAD
- Conservation de l'historique en cookie ou localstorage pour que le user voit ses saises
- Un design moderne et responsive (mobile first)