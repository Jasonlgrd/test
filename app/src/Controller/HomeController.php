<?php

namespace App\Controller;

use App\Entity\History;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class HomeController
 * @package App\Controller
 */
class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     * @return Response
     */
    public function index(Request $request, EntityManagerInterface $entityManager)
    {
        $result = 0;

        $valueOne = $request->query->get('value-one');
        $currencyOne = $request->query->get('currency-one');
        $valueTwo = $request->query->get('value-two');
        $currencyTwo = $request->query->get('currency-two');

        if ($currencyOne === $currencyTwo) {
            $result = $valueOne + $valueTwo;
        } else {
            $result = ($valueOne * 0.90) + $valueTwo;
        }

        $history = new History();
        $history->setValueOne($valueOne);
        $history->setCurrencyOne($currencyOne);
        $history->setValueTwo($valueTwo);
        $history->setCurrencyTwo($currencyTwo);
        $history->setResult($result);
        $history->setCreatedAt(new \DateTime('NOW'));
        $history->setCreatedDateAt(new \DateTime('NOW'));
        $entityManager->persist($history);
        $entityManager->flush();

        return $this->render('home.html.twig', ['result' => $result]);
    }
}