<?php

namespace App\Entity;

use App\Repository\HistoryRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=HistoryRepository::class)
 */
class History
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $valueOne;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $currencyOne;

    /**
     * @ORM\Column(type="integer")
     */
    private $valueTwo;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $currencyTwo;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $result;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="date")
     */
    private $createdDateAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getValueOne(): ?int
    {
        return $this->valueOne;
    }

    public function setValueOne(int $valueOne): self
    {
        $this->valueOne = $valueOne;

        return $this;
    }

    public function getCurrencyOne(): ?string
    {
        return $this->currencyOne;
    }

    public function setCurrencyOne(string $currencyOne): self
    {
        $this->currencyOne = $currencyOne;

        return $this;
    }

    public function getValueTwo(): ?int
    {
        return $this->valueTwo;
    }

    public function setValueTwo(int $valueTwo): self
    {
        $this->valueTwo = $valueTwo;

        return $this;
    }

    public function getCurrencyTwo(): ?string
    {
        return $this->currencyTwo;
    }

    public function setCurrencyTwo(string $currencyTwo): self
    {
        $this->currencyTwo = $currencyTwo;

        return $this;
    }

    public function getResult(): ?string
    {
        return $this->result;
    }

    public function setResult(string $result): self
    {
        $this->result = $result;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCreatedDateAt(): ?\DateTimeInterface
    {
        return $this->createdDateAt;
    }

    public function setCreatedDateAt(\DateTimeInterface $createdDateAt): self
    {
        $this->createdDateAt = $createdDateAt;

        return $this;
    }
}
