<?php

namespace App\Command;

use App\Entity\History;
use App\Repository\HistoryRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

/**
 * Class SendHistoryCommand
 * @package App\Command
 */
class SendHistoryCommand extends Command
{
    /**
     * @var string
     */
    protected static $defaultName = 'send-history';

    /**
     * @var HistoryRepository
     */
    private HistoryRepository $historyRepository;

    /**
     * @var MailerInterface
     */
    private MailerInterface $mailer;

    /**
     * SendHistoryCommand constructor.
     * @param HistoryRepository $historyRepository
     * @param MailerInterface $mailer
     * @param string|null $name
     */
    public function __construct(
        HistoryRepository $historyRepository,
        MailerInterface $mailer,
        string $name = null
    ) {
        parent::__construct($name);
        $this->historyRepository = $historyRepository;
        $this->mailer = $mailer;
    }

    /**
     *
     */
    protected function configure()
    {
        $this->setDescription('Command for send history end day');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws TransportExceptionInterface
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $histories = $this->historyRepository->getTodayQuery()->getQuery()->getResult();

        $body = '';
        /** @var History $history */
        foreach ($histories as $history) {
            $body .= '<p>' . $history->getValueOne() . $history->getCurrencyOne() . ' + ' . $history->getValueTwo() . $history->getCurrencyTwo() . ' = ' . $history->getResult() . '. Fait le : ' . $history->getCreatedAt()->format('d/m/Y H:i:s') . '</p>';
        }

        $email = (new Email())
            ->from('admin@app.com')
            ->to('client@app.com')
            ->subject('Historique de la journée')
            ->html($body);
        $this->mailer->send($email);

        return Command::SUCCESS;
    }
}
